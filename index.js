const SpotifyWebApi = require('spotify-web-api-node');
const express = require('express');

const scopes = [
  'ugc-image-upload',
  'user-read-playback-state',
  'user-modify-playback-state',
  'user-read-currently-playing',
  'streaming',
  'app-remote-control',
  'user-read-email',
  'user-read-private',
  'playlist-read-collaborative',
  'playlist-modify-public',
  'playlist-read-private',
  'playlist-modify-private',
  'user-library-modify',
  'user-library-read',
  'user-top-read',
  'user-read-playback-position',
  'user-read-recently-played',
  'user-follow-read',
  'user-follow-modify'
];

const spotifyApi = new SpotifyWebApi({
  redirectUri: 'http://localhost:8888/callback',
  clientId: "0576f004de48417da445b7c36fc56c0e",
  clientSecret: "78bf0c0ac1b44ac7b65b161785138b35"
});

const playlist1 = "37i9dQZF1DWXJfnUiYjUKT"; // New Music Friday
const playlist2 = "37i9dQZF1DWXKHMV3FjdNM"; // New Music Friday Portugal
const playlist3 = "37i9dQZF1DX6PYajiT4pAf"; // Novedades Viernes España

const moods = {
  chill: {
    tempo: {
      min: 80.00,
      max: 120.00,
    },
    danceability: {
      min: 0.5,
      max: 0.8,
    },
  },
  uptempo: {
    tempo: {
      min: 130.00,
      max: 180.00,
    },
    danceability: {
      min: 0.7,
      max: 1.0,
    },
  }
}
const app = express();

app.get('/login', (req, res) => {
  res.redirect(spotifyApi.createAuthorizeURL(scopes));
});

app.get('/callback', (req, res) => {
  const error = req.query.error;
  const code = req.query.code;

  if (error) {
    console.error('Callback Error:', error);
    res.send(`Callback Error: ${error}`);
    return;
  }

  spotifyApi
    .authorizationCodeGrant(code)
    .then(data => {
      const access_token = data.body['access_token'];
      const refresh_token = data.body['refresh_token'];
      const expires_in = data.body['expires_in'];

      spotifyApi.setAccessToken(access_token);
      spotifyApi.setRefreshToken(refresh_token);

      console.log('access_token:', access_token);
      console.log('refresh_token:', refresh_token);

      console.log(
        `Sucessfully retreived access token. Expires in ${expires_in} s.`
      );
      res.send('Success! You can now close the window.');

      setInterval(async () => {
        const data = await spotifyApi.refreshAccessToken();
        const access_token = data.body['access_token'];

        console.log('The access token has been refreshed!');
        console.log('access_token:', access_token);
        spotifyApi.setAccessToken(access_token);
      }, expires_in / 2 * 1000);
    })
    .catch(error => {
      console.error('Error getting Tokens:', error);
      res.send(`Error getting Tokens: ${error}`);
    });
});

app.get('/playlists', (req, res) => {
  const mood = {
    "chill": "https://open.spotify.com/playlist/4kQPD5KO82xwZLocoj3M9b?si=y4zFDEpxTxyZm6OytDqQFw",
    "upTempo": "https://open.spotify.com/playlist/52McmkGQDqheLMciIpLT4u?si=MdgM9SoyT6KkS4VV_zCDkg"
  }
  res.json(mood);
});

app.get('/playlist', async (req, res) => {
  // returns the playlist info
  try {
    const playlistInfo1 = await spotifyApi.getPlaylist(playlist1);
    const playlistInfo2 = await spotifyApi.getPlaylist(playlist2);
    const playlistInfo3 = await spotifyApi.getPlaylist(playlist3);
    const playlist1Tracks = playlistInfo1.body.tracks.items;
    const playlist2Tracks = playlistInfo2.body.tracks.items;
    const playlist3Tracks = playlistInfo3.body.tracks.items;
  
    const sumArray = [...playlist1Tracks, ...playlist2Tracks, ...playlist3Tracks];
  
    // Extracting the id's from each track
    const ids = new Array();
        
    sumArray.map((res) => {
      res.track && res.track.id ? ids.push(res.track.id) : '';
    });

    // Get each track feature to be able to filter by mood
    const featureTracks = await spotifyApi.getAudioFeaturesForTracks(ids.slice(0, 100));
  
    const playlistChill = new Array();
    const playlistUpTempo = new Array();

    const chill = filterByMood(featureTracks.body.audio_features, 'chill');
    const upTempo = filterByMood(featureTracks.body.audio_features, 'uptempo');

    // Add the filtered songs to each playlist
    chill.map((value) => {
      playlistChill.push(value.uri);
    });
    upTempo.map((value) => {
      playlistUpTempo.push(value.uri);
    });
    
    // Creating a playlist with the first 10 tracks
    const newPlaylist = await spotifyApi.createPlaylist('My playlist chill', { 'description': new Date().toLocaleDateString(), 'public': true });
    const newPlaylist2 = await spotifyApi.createPlaylist('My playlist uptempo', { 'description': new Date().toLocaleDateString(), 'public': true });
  
    // Add tracks to a playlist
    await spotifyApi.addTracksToPlaylist(newPlaylist.body.id, playlistChill)
    await spotifyApi.addTracksToPlaylist(newPlaylist2.body.id, playlistUpTempo)
    
    res.json(featureTracks);
  }
  catch(err) {
    res.json(err);
  }
});

function filterByMood(tracksFeatures, mood) {
  const moodParams = moods[mood];

  const filteredTracksFeatures = tracksFeatures.filter((trackFeatures) => {
    return Object.keys(moodParams).every(param => {
      return (
        parseFloat(trackFeatures[param]) <= parseFloat(moodParams[param].max) &&
        parseFloat(trackFeatures[param]) >= parseFloat(moodParams[param].min)
      );
    })
  });

  return filteredTracksFeatures;
}

app.listen(8888, () =>
  console.log(
    'HTTP Server up. Now go to http://localhost:8888/login in your browser.'
  )
);
